from svgpathtools import wsvg, Line, QuadraticBezier, Path, parse_path, Arc, CubicBezier

class Parallax:
  def __init__(self, xs, ys):
    self.xs = xs
    self.ys = ys
  
  def __call__(self, x, y, z):
    return x + self.xs*z + y*1j + self.ys*z*1j

  def face_as_lines(self, *pts):
    p_head, *p_tail = [self(*p) for p in pts]
    for p1, p2 in zip([p_head] + p_tail, p_tail + [p_head]):
      yield Line(p1, p2)


  def cube_as_lines(self, x1, x2, y1, y2, z1, z2, **kwargs):
    yield list(self.face_as_lines((x1, y1, z1),(x1, y2, z1),(x2, y2, z1),(x2, y1, z1), **kwargs))
    yield list(self.face_as_lines((x1, y1, z1),(x1, y1, z2),(x2, y1, z2),(x2, y1, z1), **kwargs))
    yield list(self.face_as_lines((x1, y1, z1),(x1, y1, z2),(x1, y2, z2),(x1, y2, z1), **kwargs))

SIZE = 32
BORDER = 2
DEPTH = 8
PERSPECTIVE = 0.5


X2 = BORDER + DEPTH * PERSPECTIVE
X1 = SIZE - BORDER
Y1 = BORDER
Y2 = SIZE - BORDER - DEPTH * PERSPECTIVE
Z2 = 0
Z1 = DEPTH
p = Parallax(-PERSPECTIVE, PERSPECTIVE)

cube_paths = [Path(*x) for x in p.cube_as_lines(X1, X2, Y1, Y2, Z1, Z2)]

GOLD = [
    Arc(2, 2+2j, 0, 1, 0, -2), 
    Arc(-2, 2+2j, 0, 1, 0, 2), 
    Arc(0.5, 0.5+0.5j, 0, 1, 0, -0.5), 
    Arc(-0.5, 0.5+0.5j, 0, 1, 0, 0.5), 
    Arc(0.25, 0.25+0.25j, 0, 1, 0, -0.25), 
    Arc(-0.25, 0.25+0.25j, 0, 1, 0, 0.25)
    ]

SILVER = [
    Arc(2j, 1.5+2j, 0, 0, 1, -2j), 
    Arc(-2j, 3+2j, 0, 0, 0, 2j), 
    ]

COPPER = [
    Arc(2, 2+2j, 0, 1, 0, -2), 
    Arc(-2, 2+2j, 0, 1, 0, 2),
    Line(2j, 6j),
    Line(4j-2,4j+2)
]

MERCURY = [
    Arc(2, 2+2j, 0, 1, 0, -2), 
    Arc(-2, 2+2j, 0, 1, 0, 2),
    Arc(-2-4j, 2+2j, 0, 1, 0, 2-4j),
    Line(2j, 6j),
    Line(4j-2,4j+2)
]

s2 = 2**0.5

IRON = [
    Arc(2, 2+2j, 0, 1, 0, -2), 
    Arc(-2, 2+2j, 0, 1, 0, 2),
    Line(s2-s2*1j, 4-4j),
    Line(1-4j, 4-4j),
    Line(4-1j, 4-4j),
]

LEAD = [
    Line(-2j, 0),
    Line(1-1j, -1-1j),
    CubicBezier(0, 2-1j, 2+1j, 2j)
]

TIN = [
    Line(-2j, 2j),
    Line(-4, 2),
    QuadraticBezier(-4, -4j, -4-8j)
]


def scale_path_to_fit_front_cube_face(path):

    path = Path(*path)

    xmin, xmax, ymin, ymax = path.bbox()

    # width/height calc
    w = xmax - xmin
    h = ymax - ymin

    # center point calc
    cx = xmin + w/2
    cy = ymin + h/2

    size = max(w, h)
    scale = 16/size

    path = path.translated(-cx -cy*1j)

    path = path.scaled(scale)

    path = path.translated(14+ 18j)

    return [path]

def build_and_save_svg(name, symbol, colour="gold"):
  if symbol:
    paths = list(scale_path_to_fit_front_cube_face(symbol))
    loccol = "black"
  else:
    paths = []
    loccol = "grey"
  l = len(paths)
  cube_opts = dict(fill=colour,stroke_width=2, stroke=loccol, stroke_linejoin="round")
  loc_opts = dict(stroke_width=2, stroke="black", stroke_linejoin="round", stroke_linecap="round", fill="none")

  wsvg(cube_paths + paths, ["black"]*(3+l), name, attributes=[cube_opts]*3 + [loc_opts]*l)

  return cube_paths + paths, [cube_opts]*3 + [loc_opts]*l


METAL_SYMBOLS = (
    ("mercury", "#ff8fcf",MERCURY),
    ("iron", "white",IRON),
    ("silver", "cyan",SILVER),
    ("gold", "gold",GOLD),
    ("copper", "Coral",COPPER),
    ("tin", "PaleGreen",TIN),
    ("lead", "MediumSlateBlue",LEAD),
    ("void", "none", [])
)

sheet_paths = []
sheet_attr = []

for i, (name, colour, symbol) in enumerate(METAL_SYMBOLS):
    paths, attr = build_and_save_svg(f"release/sprites/{name}.svg", symbol,  colour)
    xshift = i%4
    yshift = i//4
    sheet_paths.extend([p.translated(xshift*40 + yshift*40j) for p in paths])
    sheet_attr.extend(attr)

l = len(sheet_paths)
wsvg(sheet_paths, ["black"]*l, f"release/sprites/spritesheet.svg", attributes=sheet_attr)