from datetime import datetime as dt

from pyscript import document, when
from pyweb import pydom
from dataclasses import dataclass
from collections import Counter
from functools import partial, reduce
from math import gcd

METAL_NAMES = dict(
    T="Tin",
    L="Lead",
    M="Mercury",
    I="Iron",
    S="Silver",
    C="Copper",
    G="Gold",
)


@dataclass(frozen=True)
class MetalSet:
    contents: Counter

    def __add__(self, other):
        return MetalSet(self.contents.__add__(other.contents))

    def __sub__(self, other):
        return MetalSet(self.contents.__sub__(other.contents))

    def __getitem__(self, item):
        return self.contents.__getitem__(item)

    def __setitem__(self, item, value):
        return self.contents.__setitem__(item, value)

    def contains(self, other):
        return all(self[x] >= other[x] for x in METAL_NAMES)

    def valid_as_crusible(self):
        return all(self[x] >= 0 for x in METAL_NAMES) and self.contents.total() <= 6

    def is_final(self):
        return self["G"] > 0 and all(self[x] == 0 for x in METAL_NAMES if x != "G")

    def __hash__(self):
        return hash(tuple(self.contents[x] for x in METAL_NAMES))

    @classmethod
    def build(cls, string):
        return cls(Counter(list(string)))

    @classmethod
    def empty(cls):
        return cls(Counter())

    def __repr__(self):
        return "".join(
            x * self.contents[x] for x in METAL_NAMES if self.contents[x] > 0
        )
        
    def gcd(self):
        if not self.contents.total():
            return 1
        return reduce(gcd, [x for x in self.contents.values() if x])

    def prime(self):
        g = self.gcd()
        return MetalSet(Counter({k: v//g for k,v in self.contents.items()}))


@dataclass
class Spell:
    cost: MetalSet
    create: MetalSet

    def can_apply_on(self, crusible):
        return crusible.contains(self.cost)

    def apply_crusible(self, crusible):
        return crusible - self.cost + self.create

    def possible_result(self, crusible):
        if (
            self.can_apply_on(crusible)
            and self.apply_crusible(crusible).valid_as_crusible()
        ):
            return self.apply_crusible(crusible)
        else:
            return None

    @classmethod
    def build(cls, string):
        string1, string2 = string.split(">")
        return cls(MetalSet.build(string1), MetalSet.build(string2))

    def __repr__(self):
        return f"{self.cost}>{self.create}"

    def __hash__(self):
        return hash((self.cost, self.create))


level_list = []


SPELL_TOSS = "toss"
SPELL_MULT = "mult"

def spellbook_from_string(string):
    for x in string.split(";"):
        if x.strip():
            if x.strip() == "~":
                yield SPELL_TOSS
            else:
                yield Spell.build(x.strip())


def define_levels(sys):
    for x in sys.strip().split("\n"):
        level_list.append(list(spellbook_from_string(x)))


define_levels(
    """
>T;TT>C;TC>S;SC>G
>L;LL>CL;CLL>SI;CCI>LL;SSI>G
>T;>L;TL>CS;TS>LC;CS>MT;MLC>G
>T;I>C;TT>C;CC>STI;CTT>CI;SCI>TT;SSC>GG
>T;>L;TL>CT;CT>L;CC>ST;SST>G
>TT;TTT>SS;S>MT;M>C;CS>MM;MC>IS;MI>G
>MM;M>SS;SS>II;II>MS;IIS>MTT;TI>G
M>;>L;>T;TL>MSL;SL>MT;MST>MIT;ISM>STL;SS>CLL;CCT>G
>II;I>M;MI>L;ML>S;ISS>MT;MTL>G
>LL;L>S;S>M;MM>CS;MC>LLL;LLC>MTT;TSS>MI;MLI>G
>MM;>CS;CS>I;IC>T;MTI>LL;T>SG;LLS>C;TG>G
>MM;M>II;II>C;MC>S;IS>LLC;LCS>I;LI>G
>TL;>IL;>IT;ITL>SSS;S>GG;GGG>MM;GGM>
>MG;MM>SS;SG>I;ISS>MM;ISS>MC;MMC>
>TL;>MG;MMG>MSL;MGL>MTL;GT>MLL;MST>LL;LLL>
>L;>T;LT>SS;SS>IG;IL>M;MM>S;GGG>LLL;GM>
>GLL;>GTT;LT>I;TI>L;LI>T;MI>;S>;GG>SS;SG>M
>SSS;S>GGG;GG>C;CCG>M;MM>;MSS>I;I>MSS
>MG;>TC;T>LI;C>MMI;IIC>LL;T>MMI;MLL>TT;L>G;~
"""
)

print(level_list)

METAL_PICS = {}

for k, v in METAL_NAMES.items():
    METAL_PICS[k] = pydom.create("img")
    METAL_PICS[k]._js.setAttribute("src", f"sprites/{v.lower()}.svg")
    METAL_PICS[k]._js.setAttribute("title", v.lower())
    METAL_PICS[k]._js.setAttribute("alt", v.lower())

METAL_PICS[""] = pydom.create("img")
METAL_PICS[""]._js.setAttribute("src", f"sprites/arrow.svg")

METAL_PICS[...] = pydom.create("img")
METAL_PICS[...]._js.setAttribute("src", f"sprites/void.svg")

node_crusible = pydom["#crusible"][0]
node_spells = pydom["#spells"][0]
node_win = pydom["#win"][0]
node_levels = pydom["#levellist"][0]


def create_div_from_metalset(metalset, fill=1):
    div = pydom.create("span")
    lst = list(repr(metalset))
    if fill > len(lst):
        lst.extend([...] * (fill - len(lst)))
    for i, x in enumerate(lst):
        if i and i % 3 == 0:
            div.append(pydom.create("br"))
        div.append(METAL_PICS[x].clone())
    return div


def create_div_seperator():
    div = pydom.create("span")
    div.append(METAL_PICS[""].clone())
    return div


state_hist = [MetalSet.empty()]
refl_state_hist = []


def update_crusible():
    node_crusible.html = ""
    node_crusible.append(create_div_from_metalset(state_hist[-1], fill=6))
    if state_hist[-1].is_final():
        node_win._js.showModal()


def apply_spellbook(spell):
    if spell.can_apply_on(state_hist[-1]):
        newstate = spell.apply_crusible(state_hist[-1])
        if newstate.valid_as_crusible():
            state_hist.append(newstate)
            refl_state_hist[:] = []
            update_crusible()


@when("click", "#undo")
def undo():
    if len(state_hist) > 1:
        refl_state_hist.append(state_hist.pop())
    update_crusible()


@when("click", "#redo")
def redo():
    if len(refl_state_hist) > 0:
        state_hist.append(refl_state_hist.pop())
    update_crusible()


@when("click", "#empty")
def empty():
    state_hist.append(MetalSet.empty())
    update_crusible()


def add_spellbook_slot(unique, spell):
    print(spell)
    if spell == SPELL_TOSS:
        return add_toss_button(unique)
    row = pydom.create("div", classes=["spell"])
    row.append(create_div_from_metalset(spell.cost))
    row.append(pydom.create("br"))
    row.append(create_div_seperator())
    row.append(pydom.create("br"))
    row.append(create_div_from_metalset(spell.create))
    row._js.id = f"spell_{unique}"
    node_spells.append(row)
    when("click", f"#spell_{unique}")(partial(apply_spellbook, spell))


def add_toss_button(unique):
    row = pydom.create("div", classes=["spell"])
    row._js.id = f"spell_{unique}"
    row.html = "toss"
    node_spells.append(row)
    when("click", f"#spell_{unique}")(toss)


def toss():
    if state_hist[-1].gcd() > 1:
        newstate = state_hist[-1] - state_hist[-1].prime()
        if newstate.valid_as_crusible():
            state_hist.append(newstate)
            refl_state_hist[:] = []
            update_crusible()
      

def add_level_buttons():
    for x in range(len(level_list)):
        button = pydom.create("button", classes=["levelbutton"])
        if x:
            button.html = f"level {x}"
        else:
            button.html = "tutorial"
        button._js.id = f"level_{x}"
        node_levels.append(button)

        when("click", f"#level_{x}")(partial(load_level, x))


def load_level(index):
    for node in document.querySelectorAll(".spell"):
        node.remove()
    for i, x in enumerate(level_list[index]):
        add_spellbook_slot(i, x)
    state_hist[:] = [MetalSet.empty()]
    refl_state_hist[:] = []
    update_crusible()
    document.getElementById("levelselectdialog").close()
    

add_level_buttons()
load_level(0)
